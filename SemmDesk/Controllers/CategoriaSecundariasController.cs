﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
//using Data.Contexto;
using Data.Entidades;

namespace HelpDesk.Controllers
{
    public class CategoriaSecundariasController : Controller
    {
        private HelpDeskEntities db = new HelpDeskEntities();

        // GET: CategoriaSecundarias
        public ActionResult Index()
        {
            return View(db.CategoriaSecundaria.ToList());
        }

        // GET: CategoriaSecundarias/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaSecundaria categoriaSecundaria = db.CategoriaSecundaria.Find(id);
            if (categoriaSecundaria == null)
            {
                return HttpNotFound();
            }
            return View(categoriaSecundaria);
        }

        // GET: CategoriaSecundarias/Create
        public ActionResult Create()
        {
            CategoriaPrincipal categoriaPrincipal = new CategoriaPrincipal();
            var lsCategoriaPrincipal = db.CategoriaPrincipal.ToList();
            ViewBag.CategoriaPrincipal = new SelectList(lsCategoriaPrincipal, "CategoriaPrincipalID", "Descripcion", categoriaPrincipal.CategoriaPrincipalID);
            return View();
        }

        // POST: CategoriaSecundarias/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CategoriaSecundariaID,Descripcion,CategoriaPrincipalID")] CategoriaSecundaria categoriaSecundaria)
        {
            if (ModelState.IsValid)
            {
                db.CategoriaSecundaria.Add(categoriaSecundaria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(categoriaSecundaria);
        }

        // GET: CategoriaSecundarias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaSecundaria categoriaSecundaria = db.CategoriaSecundaria.Find(id);
            if (categoriaSecundaria == null)
            {
                return HttpNotFound();
            }
            var lsCategoriaPrincipal = db.CategoriaPrincipal.ToList();
            lsCategoriaPrincipal.Add(new CategoriaPrincipal {CategoriaPrincipalID = 0, descripcion = "[..Escoja una Categoria..]" } );
            lsCategoriaPrincipal = lsCategoriaPrincipal.OrderBy(l => l.descripcion).ToList();
            ViewBag.CategoriaPrincipalID = new SelectList(lsCategoriaPrincipal, "CategoriaPrincipalID", "Descripcion", categoriaSecundaria.CategoriaPrincipalID);
            Session["valor"] = categoriaSecundaria.CategoriaPrincipal.descripcion;
            return View(categoriaSecundaria);
        }

        // POST: CategoriaSecundarias/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CategoriaSecundariaID,Descripcion,CategoriaPrincipalID")] CategoriaSecundaria categoriaSecundaria)
        {
            var lsCategoriaPrincipal = db.CategoriaPrincipal.ToList();
            lsCategoriaPrincipal.Add(new CategoriaPrincipal { CategoriaPrincipalID = 0, descripcion = "[..Escoja una Categoria..]" });
            lsCategoriaPrincipal = lsCategoriaPrincipal.OrderBy(l => l.descripcion).ToList();
            ViewBag.CategoriaPrincipalID = new SelectList(lsCategoriaPrincipal, "CategoriaPrincipalID", "Descripcion", categoriaSecundaria.CategoriaPrincipalID);
            if (ModelState.IsValid)
            {
                if (categoriaSecundaria.CategoriaPrincipalID == 0)
                {
                    return View(categoriaSecundaria);
                }
                db.Entry(categoriaSecundaria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(categoriaSecundaria);
        }

        // GET: CategoriaSecundarias/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CategoriaSecundaria categoriaSecundaria = db.CategoriaSecundaria.Find(id);
            if (categoriaSecundaria == null)
            {
                return HttpNotFound();
            }
            return View(categoriaSecundaria);
        }

        // POST: CategoriaSecundarias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CategoriaSecundaria categoriaSecundaria = db.CategoriaSecundaria.Find(id);
            db.CategoriaSecundaria.Remove(categoriaSecundaria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
