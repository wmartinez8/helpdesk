﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
//using Data.Contexto;
using Data.Entidades;
using Newtonsoft.Json;

namespace HelpDesk.Controllers
{
    [Authorize]
    public class IncidenciasController : Controller
    {
        private HelpDeskEntities db = new HelpDeskEntities();
        //private HelpDeskEntities1 db2 = new HelpDeskEntities1();
        private CategoriaPrincipal cPrincipal = new CategoriaPrincipal();
        //private Usuario usuario = new Usuario();
        private Incidencia incidencia = new Incidencia();

        // GET: Incidencias
        public ActionResult Index(string parametro = "")
        {
            var lsTecnicos = db.Tecnicos.ToList();

            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "NombreTecnico", "NombreTecnico");

            var lista = db.Incidencia.ToList();

            #region ViewBags para conteo
            //Contando las incidencias TotalIncidencias
            ViewBag.TotalIncidencias =
                 lista.Count();

            //Contando las incidencias abiertas
            ViewBag.Abiertas =
                 (from incidencia in db.Incidencia
                  where incidencia.EstadoIncidenciaID == 1
                  select incidencia)
                 .Count();

            //Contando las incidencias cerradas
            ViewBag.Cerradas =
                 (from incidencia in db.Incidencia
                  where incidencia.EstadoIncidenciaID == 2
                  select incidencia)
                 .Count();


            //Contando las incidencias reasignadas

            ViewBag.Reasignadas =
                 (from incidencia in db.Incidencia
                  where incidencia.EstadoIncidenciaID == 3
                  select incidencia)
                 .Count();


            //Contando las incidencias retrasadas

            ViewBag.Retrasadas =
                 (from incidencia in db.Incidencia
                  where incidencia.EstadoIncidenciaID == 4
                  select incidencia)
                 .Count();

            ViewBag.Prioridad =
                 (from incidencia in db.Incidencia
                  where incidencia.PrioridadID == 1
                  select incidencia)
                 .Count();

            lista.Reverse();
            ViewBag.CantidadIncidencias = lista.Count();
            #endregion
            //Para filtrar por tecnicos en la lista de Incidencias
            if (parametro == "")
            {
                //Si el usuario es Administrador podra ver todas las incidencias
                if (User.IsInRole("1"))
                {
                    int _count = db.Incidencia.ToList().Count;

                    //Estoy utilizando GetRange de la lista para cuando sean muchas solo mostrar una cantidad especifica en el index
                    lista = db.Incidencia.ToList().OrderByDescending(o => o.Fecha).ThenBy(o => o.EstadoIncidenciaID).ToList().GetRange(0, _count);
                    //var stored = db2.selectVUsuarios().ToList();

                    return View("IndexAdmin",lista);
                }
                if (User.IsInRole("2"))
                {

                    var listaTecnicos = lista.Where(x => x.Tecnicos.Identificador == User.Identity.Name.Trim()).ToList();
                    return View("IndexTecnicos",listaTecnicos);
                }
                //De lo contrario solo vera las incidencias del usuairo identificado
                else
                {
                    lista = db.Incidencia.Where(x => x.UsuarioNombre == User.Identity.Name).ToList().OrderByDescending(o => o.Fecha).ThenBy(o => o.EstadoIncidenciaID).ToList();

                    //lista = db.Incidencia.Where(x => x.Usuario.UsuarioNombre == User.Identity.Name).ToList().OrderByDescending(o => o.Fecha).ThenBy(o => o.EstadoIncidenciaID).ToList();

                    return View(lista);
                }
            }
            else if (parametro != "")
            {

                lista = db.Incidencia.Where(x => x.Tecnicos.NombreTecnico == parametro).OrderBy(l => l.EstadoIncidenciaID).ToList();
            }
           
            return View(lista.ToList());
        }

        // GET: Incidencias/Details/5
        public ActionResult Details(int? id)
        {

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Incidencia incidencia = db.Incidencia.Find(id);
            if (incidencia == null)
            {
                return HttpNotFound();
            }
            if (User.IsInRole("1") || User.IsInRole("2"))
               return View("DetailsTecnicos", incidencia);
            return View(incidencia);
        }

        // GET: Incidencias/Create
        public ActionResult Create()
        {
            Incidencia incidencia = new Incidencia();

            //Creo ViewBags para mandar el nombre de departamento y usuario que estan identicado a la vista

            #region Usuarios y Departamento de la otra BD
            //  var qDepartamento = (from u in db.Usuario
            //                       join d in db.Departamentos
            //                       on u.DepartamentoID equals d.DepartamentoID
            //                       where u.UsuarioNombre == User.Identity.Name.ToString()
            //                       select d.Descripcion
            //);

            //  var qUsuario = (from u in db.Usuario
            //                  join d in db.Departamentos
            //                  on u.DepartamentoID equals d.DepartamentoID
            //                  where u.UsuarioNombre == User.Identity.Name.ToString()
            //                  select u.UsuarioNombre
            //     );
            #endregion

            Departamentos departamentos = new Departamentos();
            var usuarios = db.selectVUsuarios();
            var usuario = db.selectVUsuarios().FirstOrDefault(u => u.usuario == User.Identity.Name);
            var nombreUsuario = usuario.FullName.Trim();
            var departamento = db.selectVUsuarios().Where(d => d.FullName == nombreUsuario);
            ViewBag.nombreUsuario = nombreUsuario;
            ViewBag.nombreDepartamento = departamento.ToString();



            // Creo ViewBags para mandar listas a la vista de crear
            #region ViewBags
            //Para organizar la lista del ViewBag de la Categoria Principal
            var lista = db.CategoriaPrincipal.ToList();
            lista.Add(new CategoriaPrincipal { CategoriaPrincipalID = 0, descripcion = "[..Seleccione una Categoria..]" });
            lista = lista.OrderBy(c => c.descripcion).ToList();
            ViewBag.CategoriaPrincipalID = new SelectList(lista, "CategoriaPrincipalID", "descripcion", incidencia.CategoriaPrincipalID);

            //Para organizar la lista del ViewBag para Tecnicos
            var lsTecnicos = db.Tecnicos.ToList();
            lsTecnicos.Add(new Tecnicos { TecnicoID = 0, NombreTecnico = "[..Seleccione un Tecnico..]" });
            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "TecnicoID", "NombreTecnico", incidencia.TecnicoID);

            //Para organizar la lista del ViewBag para Departamentos
            var lsDepartamentos = db.Departamentos.ToList();
            lsDepartamentos.Add(new Departamentos { DepartamentoID = 0, Descripcion = "[..Seleccione un Departamento..]" });
            lsDepartamentos = lsDepartamentos.OrderBy(c => c.Descripcion).ToList();
            ViewBag.DepartamentoID = new SelectList(lsDepartamentos, "DepartamentoID", "Descripcion", incidencia.DepartamentoID);

            //  ViewBag.CategoriaSecundariaID = new SelectList(db.CategoriaSecundaria, "CategoriaSecundariaID", "Descripcion");
            ViewBag.EstadoIncidenciaID = new SelectList(db.EstadoIncidencia, "EstadoIncidenciaId", "Descripcion");
            ViewBag.PrioridadID = new SelectList(db.Prioridad, "PrioridadID", "Descripcion");
            ViewBag.UsuarioID = new SelectList(db.Usuario, "UsuarioID", "UsuarioNombre");

            #endregion
            return View();
        }

        // POST: Incidencias/Create
        
        [HttpPost]     
        public ActionResult Create(Incidencia incidencia)
        {
            //Como este enviando el DepartamentoID y el UsuarioID como 0 desde la vista; hago un query para darle el 
            //valor segun el usuario identidificado
            #region
            var usuario = db.selectVUsuarios().Where(u => u.usuario == User.Identity.Name).ToList();

            //var qUsuarioID = db.selectVUsuarios().Where(u => u.usuario == User.Identity.Name).ToList();
            
            #endregion

            if (ModelState.IsValid)
            {
                if (incidencia.DepartamentoID == 0)
                {
                    incidencia.DepartamentoID =(int)usuario.FirstOrDefault().departam;
                    incidencia.UsuarioID = usuario.FirstOrDefault().codigo; 
                }

                incidencia.Fecha = DateTime.Now;
                if (incidencia.PrioridadID == null)
                {
                    incidencia.PrioridadID = 2;
                }
                if (incidencia.TecnicoID == null || incidencia.TecnicoID == 0)
                {
                    incidencia.TecnicoID = 3;
                }
                if (incidencia.EstadoIncidenciaID == null)
                {
                    incidencia.EstadoIncidenciaID = 1;
                }

                incidencia.UsuarioNombre = usuario.FirstOrDefault().usuario.Trim();
               
                db.Incidencia.Add(incidencia);
                db.SaveChanges();
                ViewBag.Mensaje = "Gracias!";
                return RedirectToAction("Index");
            }

            #region
            //Para organizar la lista del ViewBag de la Categoria Principal
            var lista = db.CategoriaPrincipal.ToList();
            //lista.Add(new CategoriaPrincipal { CategoriaPrincipalID = 0, descripcion = "[..Seleccione una Categoria..]" });
            lista = lista.OrderBy(c => c.descripcion).ToList();
            ViewBag.CategoriaPrincipalID = new SelectList(lista, "CategoriaPrincipalID", "descripcion", incidencia.CategoriaPrincipalID);

            //Para organizar la lista del ViewBag para Tecnicos
            var lsTecnicos = db.Tecnicos.ToList();
            lsTecnicos.Add(new Tecnicos { TecnicoID = 0, NombreTecnico = "[..Seleccione un Tecnico..]" });
            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "TecnicoID", "NombreTecnico", incidencia.TecnicoID);

            //Para organizar la lista del ViewBag para Departamentos
            var lsDepartamentos = db.Departamentos.ToList();
            lsDepartamentos.Add(new Departamentos { DepartamentoID = 0, Descripcion = "[..Seleccione un Departamento..]" });
            lsDepartamentos = lsDepartamentos.OrderBy(c => c.Descripcion).ToList();
            ViewBag.DepartamentoID = new SelectList(lsDepartamentos, "DepartamentoID", "Descripcion", incidencia.TecnicoID);

            //  ViewBag.CategoriaSecundariaID = new SelectList(db.CategoriaSecundaria, "CategoriaSecundariaID", "Descripcion");
            ViewBag.EstadoIncidenciaID = new SelectList(db.EstadoIncidencia, "EstadoIncidenciaId", "Descripcion");
            ViewBag.PrioridadID = new SelectList(db.Prioridad, "PrioridadID", "Descripcion");
            ViewBag.UsuarioID = new SelectList(db.Usuario, "UsuarioID", "UsuarioNombre");
            #endregion

            return View(incidencia);
        }

        public JsonResult lsSecundaria(int CategoriaPrincipalID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<CategoriaSecundaria> lsCategoriaSecundaria = db.CategoriaSecundaria.Where(x => x.CategoriaPrincipalID == CategoriaPrincipalID).ToList();
            return Json(lsCategoriaSecundaria, JsonRequestBehavior.AllowGet);
        }
        
        public JsonResult lsUsuarios(int DepartamentoID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var lsUsuarios = db.selectVUsuarios()
                            .Where(u => u.departam == DepartamentoID).OrderBy(u => u.FullName).ToList();
   
            string result = JsonConvert.SerializeObject(lsUsuarios, Formatting.None);

            //List<Usuario> lsUsuarios = db.Usuario.Where(x => x.DepartamentoID == DepartamentoID).ToList();
            return Json(lsUsuarios, JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetIncidencias()
        {
            IEnumerable<vIncidencias> incidencias = new List<vIncidencias>();
            if (User.IsInRole("1"))
            {
                 incidencias = db.vIncidencias.ToList().OrderBy(x => x.Fecha).ThenBy(x => x.Estado);

            }
            if (User.IsInRole("2"))
            {
                incidencias = db.vIncidencias.Where(x => x.Tecnico == User.Identity.Name).ToList().OrderBy(x => x.Fecha).ThenBy(x => x.Estado);

            }
            if (User.IsInRole("3"))
            {
                incidencias = db.vIncidencias.Where(x => x.Usuario == User.Identity.Name).ToList().OrderBy(x => x.Fecha).ThenBy(x => x.Estado);

            }
            var ind = db.Incidencia.ToList();
            foreach (var item in ind)
            {
                var fechaInicial = item.Fecha;
                var fechaFinal = DateTime.Now;
                var fechaR = fechaFinal - fechaInicial;
                var time = fechaR.Value.Minutes;
                if (time >= 15)
                {
                    var incidenciaR = db.Incidencia.Where(x => x.IncidenciaId == item.IncidenciaId);

                    var incidenciaDB = incidenciaR.FirstOrDefault();
                    if (incidenciaDB.EstadoIncidenciaID != 2)
                    {
                        if (incidenciaDB.EstadoIncidenciaID != 4)
                        {
                            incidenciaDB.IncidenciaId = item.IncidenciaId;
                            incidenciaDB.EstadoIncidenciaID = 4;
                            incidenciaDB.UsuarioID = item.UsuarioID;
                            db.Entry(incidenciaDB).State = EntityState.Modified;
                            db.SaveChanges();
                        }                        
                    }
                }
                
            }
            
            return Json(incidencias, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Menu()
        {
            var model = db.Menu.ToList();
            return View(model);
        }

        // GET: Incidencias/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Incidencia incidencia = db.Incidencia.FirstOrDefault(i => i.IncidenciaId == id);
            if (incidencia == null)
            {
                return HttpNotFound();
            }
            var lsTecnicos = db.Tecnicos.ToList();
            lsTecnicos.Add(new Tecnicos { TecnicoID = 0, NombreTecnico = "[..Seleccione un Tecnico..]" });
            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "TecnicoID", "NombreTecnico", incidencia.TecnicoID);
            //       ViewBag.CategoriaPrincipalID = new SelectList(db.CategoriaPrincipal, "CategoriaPrincipalID", "descripcion", incidencia.CategoriaPrincipalID);
            //         ViewBag.CategoriaSecundariaID = new SelectList(db.CategoriaSecundaria, "CategoriaSecundariaID", "Descripcion", incidencia.CategoriaSecundariaID);
            ViewBag.EstadoIncidenciaID = new SelectList(db.EstadoIncidencia, "EstadoIncidenciaId", "Descripcion", incidencia.EstadoIncidenciaID);
            //       ViewBag.PrioridadID = new SelectList(db.Prioridad, "PrioridadID", "Descripcion", incidencia.PrioridadID);
            //         ViewBag.UsuarioID = new SelectList(db.Usuario, "UsuarioID", "UsuarioNombre", incidencia.UsuarioID);
            //Para organizar la lista del ViewBag para Tecnicos
            //     var lsTecnicos = db.Tenicos.ToList();     
            //     lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            //    ViewBag.TecnicoID = new SelectList(lsTecnicos, "TecnicoID", "NombreTecnico", incidencia.TecnicoID);
            return View(incidencia);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IncidenciaId,DepartamentoID,CategoriaPrincipalID,CategoriaSecundariaID,Fecha,Descripcion,PrioridadID,EstadoIncidenciaID,UsuarioID,TecnicoID,ComentarioCierre,FechaCierre,UltimaModificacion")] Incidencia incidencia)
        {
            if (ModelState.IsValid)
            {
                if (incidencia.EstadoIncidenciaID == 2)
                {
                    incidencia.FechaCierre = DateTime.Now;
                }

                if (incidencia.TecnicoID == 0 || incidencia.TecnicoID == null)
                {
                    var qUsuario = (from u in db.Tecnicos
                                    where u.Identificador == User.Identity.Name.ToString()
                                    select u.TecnicoID);
                    incidencia.TecnicoID = qUsuario.First();
                    
                }
          
                incidencia.UltimaModificacion = DateTime.Now;
                db.Entry(incidencia).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CategoriaPrincipalID = new SelectList(db.CategoriaPrincipal, "CategoriaPrincipalID", "descripcion", incidencia.CategoriaPrincipalID);
            ViewBag.CategoriaSecundariaID = new SelectList(db.CategoriaSecundaria, "CategoriaSecundariaID", "Descripcion", incidencia.CategoriaSecundariaID);
            ViewBag.EstadoIncidenciaID = new SelectList(db.EstadoIncidencia, "EstadoIncidenciaId", "Descripcion", incidencia.EstadoIncidenciaID);
            ViewBag.PrioridadID = new SelectList(db.Prioridad, "PrioridadID", "Descripcion", incidencia.PrioridadID);
            ViewBag.UsuarioID = new SelectList(db.Usuario, "UsuarioID", "CodEmp", incidencia.UsuarioID);
            return View(incidencia);
        }

        public ActionResult verAsignadas()
        {          
            var lsAsignadas = db.Incidencia.Where(i => i.EstadoIncidenciaID == 3).ToList();
            lsAsignadas = lsAsignadas.OrderBy(i => i.Tecnicos.NombreTecnico).ToList();
            return View(lsAsignadas.ToList());
        }

        public ActionResult VerAbiertas()
        {
            var lsAbiertas = db.Incidencia.Where(i => i.EstadoIncidenciaID == 1).ToList();
            lsAbiertas = lsAbiertas.OrderBy(i => i.Tecnicos.NombreTecnico).ToList();
            return View(lsAbiertas.ToList());
        }

        public ActionResult VerCerradas()
        {
            var lsCerradas = db.Incidencia.Where(i => i.EstadoIncidenciaID == 2).ToList();
            lsCerradas = lsCerradas.OrderBy(i => i.Tecnicos.NombreTecnico).ToList();
            return View(lsCerradas.ToList());
        }

        public ActionResult VerPrioridad()
        {
            var lsPrioridad = db.Incidencia.Where(i => i.PrioridadID == 1).ToList();
            lsPrioridad = lsPrioridad.OrderBy(i => i.Tecnicos.NombreTecnico).ToList();
            return View(lsPrioridad.ToList());
        }
        
        // GET: Incidencias/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Incidencia incidencia = db.Incidencia.Find(id);
        //    if (incidencia == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(incidencia);
        //}

        // POST: Incidencias/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Incidencia incidencia = db.Incidencia.Find(id);
        //    db.Incidencia.Remove(incidencia);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
