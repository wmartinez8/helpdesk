﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using Web_MVC.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using SemmDesk.Helpers;
using System.Threading.Tasks;
using RepositorioGenerico;
using Data;
using Data.Entidades;
using SemmDesk.Models;

namespace SemmDesk.Controllers
{
    public class AccountController : Controller
    {
       HelpDeskEntities contexto = new HelpDeskEntities();
        // GET: Account
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(LoginViewModel data, string returnUrl)
        {
            ActionResult Result;
            string clave = Funciones.Encrypt(data.Clave);
            RepositorioGenerico.Repositorio<Usuario> Usuario =
                new RepositorioGenerico.Repositorio<Usuario>(contexto);

            var User = contexto.Usuario.Where(x => x.Clave == data.Clave.ToString()).FirstOrDefault();

            if (User != null)
            {
                Result = SignInUser(User, returnUrl);
            }
            else
            {
                Result = View(data);
            }
            return Result;
        }

        private ActionResult SignInUser(Usuario user, string returnUrl)
        {
            ActionResult Result;

            List<Claim> Claims = new List<Claim>();

            Claims.Add(new Claim(ClaimTypes.NameIdentifier, user.UsuarioID.ToString()));

            //Claims.Add(new Claim(ClaimTypes.Email, user.Email));
            Claims.Add(new Claim(ClaimTypes.Role, user.RolID.ToString()));

            Claims.Add(new Claim(ClaimTypes.Name, user.UsuarioNombre));
    
            Claims.Add(new Claim("DepartamentoID", $"{user.DepartamentoID}"));

            //if (user.Rol != null && user.Rol.Any())
            //{
            //    Claims.AddRange(user.Rol.Select(
            //        x => new Claim(ClaimTypes.Role, x.RolID.ToString())));
            //}

            var Identity = new ClaimsIdentity(Claims, DefaultAuthenticationTypes.ApplicationCookie);

            IAuthenticationManager authenticationManager = 
                HttpContext.GetOwinContext().Authentication;

            authenticationManager.SignIn(
                new AuthenticationProperties()
                { }, Identity);

            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                returnUrl = Url.Action("Index", "Incidencias");
            }

            Result = Redirect(returnUrl);
            return Result;
        }

        public ActionResult LogOff()
        {
            IAuthenticationManager authenticationManager =
                HttpContext.GetOwinContext().Authentication;

            authenticationManager.SignOut(
                DefaultAuthenticationTypes.ApplicationCookie);

            return RedirectToAction("Index", "Home");
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLinkLogin(string provider, string returnUrl)
        //{
        //    string UserID = null;
        //    // Obtenemos el identificador del usuario autenticado
        //    if (this.User.Identity.IsAuthenticated && User is ClaimsPrincipal)
        //    {
        //        var Identity = User as ClaimsPrincipal;
        //        var Claims = Identity.Claims.ToList();
        //        UserID = Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
        //    }
        //    // Solicitamos un Redirect al proveedor externo
        //    return new ChallengeResult(provider, Url.Action(
        //        "ExternalLinkLoginCallback", "Account", new { ReturnUrl = returnUrl }), UserID);
        //}

        //public async Task<ActionResult> ExternalLinkLoginCallback()
        //{
        //    ActionResult Result;
        //    // Obtener la información devuelta por el proveedor externo
        //    var LoginInfo =
        //        await HttpContext.GetOwinContext().
        //        Authentication.GetExternalLoginInfoAsync(
        //            ChallengeResult.XsrfKey, User.Identity.GetUserId());

        //    if (LoginInfo == null)
        //        Result = Content("No se pudo realizar la autenticación con el proveedor externo");
        //    else
        //    {
        //        // El usuario ha sido autenticado por el proveedor externo!
        //        // Obtener la llave del proveedor de autenticación.
        //        // Esta llave es específica del usuario.
        //        string ProviderKey = LoginInfo.Login.ProviderKey;
        //        // Obtener el nombre del proveedor de autenticación.
        //        string ProviderName = LoginInfo.Login.LoginProvider;
        //        // Enlazar los datos de la cuenta externa con la cuenta de usuario local. 
        //        int IdUsuario = int.Parse(Funciones.GetClaimInfo(ClaimTypes.NameIdentifier));
        //        //User.Identity.GetUserId<int>()
        //        Repositorio<Usuario> Usuario = new Repositorio<Usuario>(contexto);
        //        Repositorio.Excepcion += Repositorio_Excepcion;
        //        Usuario.Update(x => x.UsuarioID == IdUsuario, "ProviderKey", ProviderKey);
        //        Usuario.Update(x => x.UsuarioID == IdUsuario, "ProviderName", ProviderName);
        //        Repositorio.Excepcion -= Repositorio_Excepcion;
        //        Result = Content($"Se ha enlazado la cuenta local con la cuenta de {ProviderName}");
        //    }
        //    return Result;
        //}

        private void Repositorio_Excepcion(object sender, ExceptionEvenArgs e)
        {
            
        }

        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult ExternalLogin(string provider, string returnUrl)
        //{
        //    // Solicitamos un Redirect al proveedor externo.
        //    return new
        //        ChallengeResult(provider, Url.Action(
        //            "ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        //}

        //public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        //{
        //    ActionResult Result;

        //    // Obtener la información devuelta por el proveedor externo
        //    var LoginInfo = await HttpContext.GetOwinContext().
        //        Authentication.GetExternalLoginInfoAsync();

        //    if (LoginInfo == null)
        //        // No se pudo autenticar.
        //        Result = RedirectToAction("Login");
        //    else
        //    {
        //        // El usuario ha sido autenticado por el proveedor externo!
        //        // Obtener la llave del proveedor que identifica al usuario.
        //        string ProviderKey = LoginInfo.Login.ProviderKey;
        //        // Buscar al usuario
        //        Repositorio<Usuario> Usuario = new Repositorio<Usuario>(contexto);

        //        var User = Usuario.Retrieve(x => x.ProviderKey == ProviderKey);
        //        if (User != null)// Se ha encontrado al usuario. Iniciar la sesión del usuario.                    
        //            Result = SignInUser(User, false, returnUrl);
        //        else
        //            Result = Content($"Imposible iniciar sesión con {LoginInfo.Login.LoginProvider}");
        //    }
        //    return Result;
        //}
    }
}