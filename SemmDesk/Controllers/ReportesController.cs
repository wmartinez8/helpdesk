﻿
using Data.Entidades;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Reporting.WebForms;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using SemmDesk.Reportes;
//using SemmDesk.Reportes.DataSetIncidenciasTableAdapters;
using SemmDesk.Reportes.dsIncidenciasTableAdapters;

namespace SemmDesk.Controllers
{
    [Authorize]
    public class ReportesController : Controller
    {
        private HelpDeskEntities db = new HelpDeskEntities();
        // GET: Reportes
        public ActionResult Index()
        {
            //listar Tecnicos
            var lsTecnicos = db.Tecnicos.ToList();
            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "NombreTecnico", "NombreTecnico");

            //Instanciando el reporte, fijando tamano y modo local:
            ReportViewer reporte = new ReportViewer();
            reporte.ProcessingMode = ProcessingMode.Local;
            reporte.SizeToReportContent = true;
            reporte.Width = Unit.Pixel(9200);
            reporte.Height = Unit.Pixel(600);
           
            //Al reporte instanciado le asigno un source (una instancia de la vista Incidencia)
            vIncidenciasTableAdapter vIncidenciasTable = new vIncidenciasTableAdapter();
             
            var incidencia = vIncidenciasTable.GetData().ToList();
            reporte.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"/Reportes/rIncidencias.rdlc";

            reporte.LocalReport.DataSources.Add(new ReportDataSource("dsIncidencias", incidencia));

            ViewBag.ReportViewer = reporte;          
           
            return View(reporte);
        }


        [HttpPost]
        public ActionResult GetReport(DateTime? fInicial, DateTime? fFinal, string nombreTecnico)
        {

            var lsTecnicos = db.Tecnicos.ToList();

            lsTecnicos = lsTecnicos.OrderBy(c => c.NombreTecnico).ToList();
            ViewBag.TecnicoID = new SelectList(lsTecnicos, "NombreTecnico", "NombreTecnico");

            ReportViewer reporte = new ReportViewer();
            reporte.ProcessingMode = ProcessingMode.Local;
            reporte.SizeToReportContent = true;
            reporte.Width = Unit.Pixel(800);
            reporte.Height = Unit.Pixel(600);
            var Inicio = fInicial;
            var Final = fFinal;
            if (fInicial == null)
            {
                fInicial = Convert.ToDateTime("2018-01-01");

            }
            if (fFinal == null)
            {
                fFinal = DateTime.Now;
            }
         
           
            vIncidenciasTableAdapter vIncidenciasTable = new vIncidenciasTableAdapter();

            var incidencia = vIncidenciasTable.GetDataBy1(fInicial, fFinal, nombreTecnico).ToList();
            //var incidencia = vIncidenciasTable.GetData().ToList();
            reporte.LocalReport.ReportPath = Request.MapPath(Request.ApplicationPath) + @"/Reportes/rIncidencias.rdlc";

            reporte.LocalReport.DataSources.Add(new ReportDataSource("dsIncidencias", incidencia));

            ViewBag.ReportViewer = reporte;
            return View("Index");
        }
    }
}