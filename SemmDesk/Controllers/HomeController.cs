﻿using Data.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SemmDesk.Controllers
{
    public class HomeController : Controller
    {
        private HelpDeskEntities db = new HelpDeskEntities();
        public ActionResult Index()
        {
            var model = db.Menu.ToList();
            Session["Menu"] = model;
            return View(model);
        }

        public ActionResult Menu()
        {
            if (Session["Menu"] == null)
            {
                Session["Menu"] = db.Menu.ToList();
            }
            var model = Session["Menu"];
            return PartialView("_Menu", model);
        }
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}