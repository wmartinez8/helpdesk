﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SemmDesk.Models
{
    public class LoginViewModel
    {        
            [Required]
            [Display(Name = "Nombre")]
            public string Nombre { get; set; }

            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Clave")]
            public string Clave { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
       
    }
}